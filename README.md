# Metaseminar hands-on 2023-04-28

Container for Metaseminar hands-on 2023-04-28

## Hands-on Horizon

Using the OpenStack personal project.

### 1. Generate SSH keypair
You may need to have testing SSH key pair
```sh
SSH_KEYPAIR_DIR="${HOME}/.ssh/generated-keypair"
mkdir -p ${SSH_KEYPAIR_DIR}
chmod 700 ${SSH_KEYPAIR_DIR}
ssh-keygen -t rsa -b 4096 -f "${SSH_KEYPAIR_DIR}/id_rsa.demo"
ls -la ${SSH_KEYPAIR_DIR}/id_rsa.demo*
```
### 2. [Horizon UI login](https://dashboard.cloud.muni.cz)
### 3. Register new SSH pubkey ([Compute -> Key Pairs](https://dashboard.cloud.muni.cz/project/key_pairs))
### 4. Create VM ([Compute -> Instances -> Launch instance](https://dashboard.cloud.muni.cz/project/instances/))
1. Details subpage: Specify Instance name.
1. Source subpage: Select boot source image or existing bootable volume.
1. Flavor subpage: Pick one of available (standard) flavors.
1. Networks subpage: Pick one of the pre-created personal project networks.  147-251-115-pers-proj-net
1. Network ports subpage: skip
1. Security groups subpage: Pick default.
1. Key Pair subpage: Pick created above keypair.
1. Configuration subpage allows to define cloud-init configuration. Skip and Launch instance.

### 5. VM inspection Compute -> Instances -> Pick instance

* Overview
  * Name & ID
  * Spec i.e. flavor
  * Security Groups, verify existing ingress rules
  * Volumes attached
* Interfaces
  * selected network
* (Console) Log
  * inspect cloud-init modifications

### 6. Associating FIP public IPv4 address

### 7. Associating public IPv6 address

### 8. Generating Application Credentials ([Identity -> Application Credentials](https://dashboard.cloud.muni.cz/identity/application_credentials/))


## Hands-on command-line client in group project

```sh
# docker run -it --rm registry.gitlab.ics.muni.cz:443/246254/metaseminar-hands-on-2023-04-28/hands-on-tools:latest
source /tmp/ac/prod-metaseminar-hands-on-2023-04-28-openrc.sh.inc
openstack version show | grep identity
cd openstack-infrastructure-as-code-automation/clouds/g1/brno/meta-metaseminar-hands-on-2023-04-28/commandline/
./cmdline-demo-group-project.sh freznicek-demo # use your own name as custom infrastructure prefix in the single hands-on project
```

## Hands-on terraform

```sh
# docker run -it --rm registry.gitlab.ics.muni.cz:443/246254/metaseminar-hands-on-2023-04-28/hands-on-tools:latest

# generate in-container ssh keypair
ssh-keygen -t rsa -b 4096
...
# read-in the openstack credentials
source /tmp/ac/prod-metaseminar-hands-on-2023-04-28-openrc.sh.inc

# enter terraform workspace
cd openstack-infrastructure-as-code-automation/clouds/g1/brno/meta-metaseminar-hands-on-2023-04-28/terraform_group_project/

# change infrastructure prefix
mcedit main.tf     # kusername = freznicek

# initial deploy via terraform
terraform init
terraform validate
terraform plan --out plan
terraform apply plan

# login to VM
ncat -z <ip-address> 22
ssh ubuntu@<ip-address>

# doublecheck in horizon in meta-metaseminar-hands-on-2023-04-28 project
# https://dashboard.cloud.muni.cz

# scaling 1->3 VMs
# https://gitlab.ics.muni.cz/cloud/g2/openstack-infrastructure-as-code-automation/-/blob/8c66c1502f2cba26cf9dd51e89c118966ba5e6ed/clouds/g1/brno/meta-metaseminar-hands-on-2023-04-28/terraform_group_project/main.tf#L25
mcedit main.tf     # nodes_count = 3
terraform validate
terraform plan --out plan
terraform apply plan

# doublecheck in horizon in meta-metaseminar-hands-on-2023-04-28 project
# https://dashboard.cloud.muni.cz

# delete VM via Horizon, and re-deploy via Horizon
# * point out idential internal IP address
terraform validate && terraform plan --out plan && terraform apply plan

# scaling 3->1 VMs
# https://gitlab.ics.muni.cz/cloud/g2/openstack-infrastructure-as-code-automation/-/blob/8c66c1502f2cba26cf9dd51e89c118966ba5e6ed/clouds/g1/brno/meta-metaseminar-hands-on-2023-04-28/terraform_group_project/main.tf#L25
mcedit main.tf     # nodes_count = 1
terraform validate && terraform plan --out plan && terraform apply plan

# doublecheck in horizon in meta-metaseminar-hands-on-2023-04-28 project
# https://dashboard.cloud.muni.cz

# two disks /dev/sd[ab]
ssh ubuntu@<ip-address> 'lsblk'

# add additional volume (not enough data)
mcedit main.tf     # sdc_volume = 1
terraform validate && terraform plan --out plan && terraform apply plan

# two disks /dev/sd[abc]
ssh ubuntu@<ip-address> 'lsblk'

# remove original volume
mcedit main.tf     # sdb_volume = 0
terraform validate && terraform plan --out plan && terraform apply plan

# two disks /dev/sd[ac]
ssh ubuntu@<ip-address> 'lsblk'

# destroy whole infrastructure
terraform destroy
```

## Build image transcript

```sh
[freznicek@lenovo-t14 containerimage 0]$ podman login -u 246254 registry.gitlab.ics.muni.cz:443
...
[freznicek@lenovo-t14 containerimage 0]$ cd /home/freznicek/tmp/containerimage
[freznicek@lenovo-t14 containerimage 0]$ podman build . -t registry.gitlab.ics.muni.cz:443/246254/metaseminar-hands-on-2023-04-28/hands-on-tools:latest --no-cache
...
done.
Processing triggers for dbus (1.12.20-2ubuntu4.1) ...
--> df480181fa9
STEP 3/9: RUN curl https://apt.releases.hashicorp.com/gpg | gpg --dearmor > hashicorp.gpg &&     install -o root -g root -m 644 hashicorp.gpg /etc/apt/trusted.gpg.d/ &&     apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main" &&     apt install terraform
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  3980  100  3980    0     0  28918      0 --:--:-- --:--:-- --:--:-- 29051
Get:1 https://apt.releases.hashicorp.com jammy InRelease [12.9 kB]
Hit:2 http://archive.ubuntu.com/ubuntu jammy InRelease
Hit:3 http://security.ubuntu.com/ubuntu jammy-security InRelease
Hit:4 http://archive.ubuntu.com/ubuntu jammy-updates InRelease
Hit:5 http://archive.ubuntu.com/ubuntu jammy-backports InRelease
Get:6 https://apt.releases.hashicorp.com jammy/main amd64 Packages [101 kB]
Fetched 114 kB in 1s (179 kB/s)
Reading package lists...
Repository: 'deb [arch=amd64] https://apt.releases.hashicorp.com jammy main'
Description:
Archive for codename: jammy components: main
More info: https://apt.releases.hashicorp.com
Adding repository.
Adding deb entry to /etc/apt/sources.list.d/archive_uri-https_apt_releases_hashicorp_com-jammy.list
Adding disabled deb-src entry to /etc/apt/sources.list.d/archive_uri-https_apt_releases_hashicorp_com-jammy.list

WARNING: apt does not have a stable CLI interface. Use with caution in scripts.

Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  terraform
0 upgraded, 1 newly installed, 0 to remove and 4 not upgraded.
Need to get 21.5 MB of archives.
After this operation, 64.4 MB of additional disk space will be used.
Get:1 https://apt.releases.hashicorp.com jammy/main amd64 terraform amd64 1.4.5-1 [21.5 MB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 21.5 MB in 2s (10.9 MB/s)
Selecting previously unselected package terraform.
(Reading database ... 24224 files and directories currently installed.)
Preparing to unpack .../terraform_1.4.5-1_amd64.deb ...
Unpacking terraform (1.4.5-1) ...
Setting up terraform (1.4.5-1) ...
--> d8a0015ec16
STEP 4/9: RUN python3 -mpip install openstackclient
Collecting openstackclient
  Downloading openstackclient-4.0.0-py2.py3-none-any.whl (7.0 kB)
Collecting python-vitrageclient>=1.3.0
  Downloading python_vitrageclient-4.7.0-py3-none-any.whl (50 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 51.0/51.0 KB 1.7 MB/s eta 0:00:00
Collecting gnocchiclient>=3.3.1
  Downloading gnocchiclient-7.0.8-py2.py3-none-any.whl (66 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 66.5/66.5 KB 3.4 MB/s eta 0:00:00
Collecting python-watcherclient>=1.1.0
  Downloading python_watcherclient-4.1.0-py3-none-any.whl (125 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 125.7/125.7 KB 12.8 MB/s eta 0:00:00
Collecting python-openstackclient>=4.0.0
  Downloading python_openstackclient-6.2.0-py3-none-any.whl (1.1 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 1.1/1.1 MB 9.3 MB/s eta 0:00:00
Collecting python-heatclient>=1.10.0
  Downloading python_heatclient-3.2.0-py3-none-any.whl (212 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 212.7/212.7 KB 13.1 MB/s eta 0:00:00
Collecting python-senlinclient>=1.1.0
  Downloading python_senlinclient-3.0.0-py3-none-any.whl (111 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 111.3/111.3 KB 12.8 MB/s eta 0:00:00
Collecting python-congressclient<2000,>=1.3.0
  Downloading python_congressclient-2.0.1-py3-none-any.whl (40 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 40.9/40.9 KB 20.3 MB/s eta 0:00:00
Collecting python-saharaclient>=1.4.0
  Downloading python_saharaclient-4.1.0-py3-none-any.whl (156 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 156.8/156.8 KB 10.4 MB/s eta 0:00:00
Collecting python-designateclient>=2.7.0
  Downloading python_designateclient-5.2.0-py3-none-any.whl (95 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 95.6/95.6 KB 14.3 MB/s eta 0:00:00
Collecting python-barbicanclient>=4.5.2
  Downloading python_barbicanclient-5.5.0-py3-none-any.whl (89 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 89.5/89.5 KB 15.2 MB/s eta 0:00:00
Collecting python-muranoclient>=0.8.2
  Downloading python_muranoclient-2.6.0-py3-none-any.whl (260 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 260.9/260.9 KB 11.4 MB/s eta 0:00:00
Collecting python-octaviaclient>=1.3.0
  Downloading python_octaviaclient-3.4.0-py3-none-any.whl (112 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 112.4/112.4 KB 11.1 MB/s eta 0:00:00
Collecting aodhclient>=0.9.0
  Downloading aodhclient-3.3.0-py3-none-any.whl (51 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 51.6/51.6 KB 22.0 MB/s eta 0:00:00
Collecting python-zunclient>=3.4.0
  Downloading python_zunclient-4.6.0-py3-none-any.whl (155 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 155.1/155.1 KB 10.5 MB/s eta 0:00:00
Collecting pbr!=2.1.0,>=2.0.0
  Downloading pbr-5.11.1-py2.py3-none-any.whl (112 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 112.7/112.7 KB 11.3 MB/s eta 0:00:00
Collecting python-troveclient>=2.2.0
  Downloading python_troveclient-8.1.0-py3-none-any.whl (237 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 237.4/237.4 KB 9.9 MB/s eta 0:00:00
Collecting python-mistralclient!=3.2.0,>=3.1.0
  Downloading python_mistralclient-5.0.0-py3-none-any.whl (143 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 143.3/143.3 KB 8.2 MB/s eta 0:00:00
Collecting networkx>=2.3
  Downloading networkx-3.1-py3-none-any.whl (2.1 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 2.1/2.1 MB 11.3 MB/s eta 0:00:00
Collecting python-searchlightclient>=1.0.0
  Downloading python_searchlightclient-2.1.1-py3-none-any.whl (44 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 44.2/44.2 KB 27.4 MB/s eta 0:00:00
Collecting python-neutronclient>=6.7.0
  Downloading python_neutronclient-9.0.0-py3-none-any.whl (429 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 429.3/429.3 KB 11.1 MB/s eta 0:00:00
Collecting python-ironic-inspector-client>=1.5.0
  Downloading python_ironic_inspector_client-4.9.0-py3-none-any.whl (37 kB)
Collecting python-ironicclient>=2.3.0
  Downloading python_ironicclient-5.1.0-py3-none-any.whl (247 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 247.7/247.7 KB 11.8 MB/s eta 0:00:00
Collecting python-zaqarclient>=1.0.0
  Downloading python_zaqarclient-2.5.1-py3-none-any.whl (87 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 87.7/87.7 KB 12.3 MB/s eta 0:00:00
Collecting oslo.utils>=2.0.0
  Downloading oslo.utils-6.1.0-py3-none-any.whl (100 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 100.5/100.5 KB 4.7 MB/s eta 0:00:00
Collecting osc-lib>=1.0.1
  Downloading osc_lib-2.7.0-py3-none-any.whl (88 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 88.9/88.9 KB 11.5 MB/s eta 0:00:00
Collecting osprofiler>=1.4.0
  Downloading osprofiler-3.4.3-py3-none-any.whl (89 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 89.6/89.6 KB 16.1 MB/s eta 0:00:00
Collecting keystoneauth1>=1.0.0
  Downloading keystoneauth1-5.1.2-py3-none-any.whl (318 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 318.7/318.7 KB 11.6 MB/s eta 0:00:00
Requirement already satisfied: pyparsing in /usr/lib/python3/dist-packages (from aodhclient>=0.9.0->openstackclient) (2.4.7)
Collecting oslo.serialization>=1.4.0
  Downloading oslo.serialization-5.1.1-py3-none-any.whl (25 kB)
Collecting oslo.i18n>=1.5.0
  Downloading oslo.i18n-6.0.0-py3-none-any.whl (46 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 46.1/46.1 KB 9.5 MB/s eta 0:00:00
Collecting cliff!=1.16.0,>=1.14.0
  Downloading cliff-4.2.0-py3-none-any.whl (81 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 81.0/81.0 KB 12.1 MB/s eta 0:00:00
Collecting ujson
  Downloading ujson-5.7.0-cp310-cp310-manylinux_2_17_x86_64.manylinux2014_x86_64.whl (52 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 52.8/52.8 KB 17.6 MB/s eta 0:00:00
Collecting iso8601
  Downloading iso8601-1.1.0-py3-none-any.whl (9.9 kB)
Collecting futurist
  Downloading futurist-2.4.1-py3-none-any.whl (36 kB)
Collecting python-dateutil
  Downloading python_dateutil-2.8.2-py2.py3-none-any.whl (247 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 247.7/247.7 KB 12.0 MB/s eta 0:00:00
Collecting debtcollector
  Downloading debtcollector-2.5.0-py3-none-any.whl (23 kB)
Requirement already satisfied: six in /usr/lib/python3/dist-packages (from gnocchiclient>=3.3.1->openstackclient) (1.16.0)
Collecting requests>=2.14.2
  Downloading requests-2.28.2-py3-none-any.whl (62 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 62.8/62.8 KB 12.5 MB/s eta 0:00:00
Collecting Babel!=2.4.0,>=2.3.4
  Downloading Babel-2.12.1-py3-none-any.whl (10.1 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 10.1/10.1 MB 11.5 MB/s eta 0:00:00
Collecting oslo.log>=3.36.0
  Downloading oslo.log-5.2.0-py3-none-any.whl (71 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 72.0/72.0 KB 20.3 MB/s eta 0:00:00
Collecting stevedore>=1.20.0
  Downloading stevedore-5.0.0-py3-none-any.whl (49 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 49.6/49.6 KB 13.2 MB/s eta 0:00:00
Collecting jsonschema>=3.2.0
  Downloading jsonschema-4.17.3-py3-none-any.whl (90 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 90.4/90.4 KB 8.9 MB/s eta 0:00:00
Collecting PrettyTable>=0.7.2
  Downloading prettytable-3.7.0-py3-none-any.whl (27 kB)
Collecting python-swiftclient>=3.2.0
  Downloading python_swiftclient-4.3.0-py3-none-any.whl (88 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 88.9/88.9 KB 21.5 MB/s eta 0:00:00
Collecting PyYAML>=3.13
  Downloading PyYAML-6.0-cp310-cp310-manylinux_2_5_x86_64.manylinux1_x86_64.manylinux_2_12_x86_64.manylinux2010_x86_64.whl (682 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 682.2/682.2 KB 11.3 MB/s eta 0:00:00
Collecting dogpile.cache>=0.8.0
  Downloading dogpile.cache-1.1.8-py3-none-any.whl (51 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 51.5/51.5 KB 12.9 MB/s eta 0:00:00
Collecting appdirs>=1.3.0
  Downloading appdirs-1.4.4-py2.py3-none-any.whl (9.6 kB)
Collecting openstacksdk>=0.18.0
  Downloading openstacksdk-1.0.1-py3-none-any.whl (1.6 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 1.6/1.6 MB 11.4 MB/s eta 0:00:00
Collecting murano-pkg-check>=0.3.0
  Downloading murano-pkg-check-0.3.0.tar.gz (39 kB)
  Preparing metadata (setup.py): started
  Preparing metadata (setup.py): finished with status 'done'
Collecting python-keystoneclient>=3.8.0
  Downloading python_keystoneclient-5.1.0-py3-none-any.whl (398 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 398.6/398.6 KB 11.8 MB/s eta 0:00:00
Collecting pyOpenSSL>=17.1.0
  Downloading pyOpenSSL-23.1.1-py3-none-any.whl (57 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 57.9/57.9 KB 15.9 MB/s eta 0:00:00
Collecting python-glanceclient>=2.8.0
  Downloading python_glanceclient-4.3.0-py3-none-any.whl (205 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 205.3/205.3 KB 12.1 MB/s eta 0:00:00
Collecting yaql>=1.1.3
  Downloading yaql-2.0.0-py3-none-any.whl (125 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 126.0/126.0 KB 13.5 MB/s eta 0:00:00
Collecting simplejson>=3.5.1
  Downloading simplejson-3.19.1-cp310-cp310-manylinux_2_5_x86_64.manylinux1_x86_64.manylinux_2_17_x86_64.manylinux2014_x86_64.whl (137 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 137.9/137.9 KB 11.9 MB/s eta 0:00:00
Collecting netaddr>=0.7.18
  Downloading netaddr-0.8.0-py2.py3-none-any.whl (1.9 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 1.9/1.9 MB 11.6 MB/s eta 0:00:00
Collecting os-client-config>=1.28.0
  Downloading os_client_config-2.1.0-py3-none-any.whl (31 kB)
Collecting python-novaclient>=18.1.0
  Downloading python_novaclient-18.3.0-py3-none-any.whl (335 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 335.9/335.9 KB 10.5 MB/s eta 0:00:00
Collecting python-cinderclient>=3.3.0
  Downloading python_cinderclient-9.3.0-py3-none-any.whl (255 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 255.9/255.9 KB 11.1 MB/s eta 0:00:00
Collecting PrettyTable>=0.7.2
  Downloading prettytable-0.7.2.zip (28 kB)
  Preparing metadata (setup.py): started
  Preparing metadata (setup.py): finished with status 'done'
Collecting pydot>=1.4.1
  Downloading pydot-1.4.2-py2.py3-none-any.whl (21 kB)
Collecting websocket-client>=0.44.0
  Downloading websocket_client-1.5.1-py3-none-any.whl (55 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 55.9/55.9 KB 12.5 MB/s eta 0:00:00
Collecting docker>=2.4.2
  Downloading docker-6.0.1-py3-none-any.whl (147 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 147.5/147.5 KB 11.0 MB/s eta 0:00:00
Collecting cmd2>=1.0.0
  Downloading cmd2-2.4.3-py3-none-any.whl (147 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 147.2/147.2 KB 11.8 MB/s eta 0:00:00
Collecting autopage>=0.4.0
  Downloading autopage-0.5.1-py3-none-any.whl (29 kB)
Requirement already satisfied: importlib-metadata>=4.4 in /usr/lib/python3/dist-packages (from cliff!=1.16.0,>=1.14.0->aodhclient>=0.9.0->openstackclient) (4.6.4)
Collecting wrapt>=1.7.0
  Downloading wrapt-1.15.0-cp310-cp310-manylinux_2_5_x86_64.manylinux1_x86_64.manylinux_2_17_x86_64.manylinux2014_x86_64.whl (78 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 78.4/78.4 KB 38.7 MB/s eta 0:00:00
Collecting packaging>=14.0
  Downloading packaging-23.1-py3-none-any.whl (48 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 48.9/48.9 KB 4.1 MB/s eta 0:00:00
Collecting urllib3>=1.26.0
  Downloading urllib3-1.26.15-py2.py3-none-any.whl (140 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 140.9/140.9 KB 20.0 MB/s eta 0:00:00
Collecting decorator>=4.0.0
  Downloading decorator-5.1.1-py3-none-any.whl (9.1 kB)
Collecting attrs>=17.4.0
  Downloading attrs-23.1.0-py3-none-any.whl (61 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 61.2/61.2 KB 10.2 MB/s eta 0:00:00
Collecting pyrsistent!=0.17.0,!=0.17.1,!=0.17.2,>=0.14.0
  Downloading pyrsistent-0.19.3-py3-none-any.whl (57 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 57.5/57.5 KB 7.4 MB/s eta 0:00:00
Collecting os-service-types>=1.2.0
  Downloading os_service_types-1.7.0-py2.py3-none-any.whl (24 kB)
Collecting semantic-version>=2.3.1
  Downloading semantic_version-2.10.0-py2.py3-none-any.whl (15 kB)
Collecting requestsexceptions>=1.2.0
  Downloading requestsexceptions-1.4.0-py2.py3-none-any.whl (3.8 kB)
Collecting netifaces>=0.10.4
  Downloading netifaces-0.11.0.tar.gz (30 kB)
  Preparing metadata (setup.py): started
  Preparing metadata (setup.py): finished with status 'done'
Requirement already satisfied: cryptography>=2.7 in /usr/lib/python3/dist-packages (from openstacksdk>=0.18.0->python-ironicclient>=2.3.0->openstackclient) (3.4.8)
Collecting jsonpatch!=1.20,>=1.16
  Downloading jsonpatch-1.32-py2.py3-none-any.whl (12 kB)
Collecting jmespath>=0.9.0
  Downloading jmespath-1.0.1-py3-none-any.whl (20 kB)
Collecting oslo.config>=5.2.0
  Downloading oslo.config-9.1.1-py3-none-any.whl (128 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 128.4/128.4 KB 11.2 MB/s eta 0:00:00
Collecting oslo.context>=2.21.0
  Downloading oslo.context-5.1.1-py3-none-any.whl (20 kB)
Collecting pyinotify>=0.9.6
  Downloading pyinotify-0.9.6.tar.gz (60 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 61.0/61.0 KB 11.1 MB/s eta 0:00:00
  Preparing metadata (setup.py): started
  Preparing metadata (setup.py): finished with status 'done'
Collecting msgpack>=0.5.2
  Downloading msgpack-1.0.5-cp310-cp310-manylinux_2_17_x86_64.manylinux2014_x86_64.whl (316 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 316.8/316.8 KB 11.5 MB/s eta 0:00:00
Collecting pytz>=2013.6
  Downloading pytz-2023.3-py2.py3-none-any.whl (502 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 502.3/502.3 KB 12.0 MB/s eta 0:00:00
Collecting WebOb>=1.7.1
  Downloading WebOb-1.8.7-py2.py3-none-any.whl (114 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 115.0/115.0 KB 12.1 MB/s eta 0:00:00
Collecting oslo.concurrency>=3.26.0
  Downloading oslo.concurrency-5.1.1-py3-none-any.whl (48 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 48.6/48.6 KB 16.7 MB/s eta 0:00:00
Collecting cryptography>=2.7
  Downloading cryptography-40.0.2-cp36-abi3-manylinux_2_28_x86_64.whl (3.7 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 3.7/3.7 MB 11.5 MB/s eta 0:00:00
Collecting warlock>=1.2.0
  Downloading warlock-2.0.1-py3-none-any.whl (9.8 kB)
Collecting charset-normalizer<4,>=2
  Downloading charset_normalizer-3.1.0-cp310-cp310-manylinux_2_17_x86_64.manylinux2014_x86_64.whl (199 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 199.3/199.3 KB 12.3 MB/s eta 0:00:00
Collecting idna<4,>=2.5
  Downloading idna-3.4-py3-none-any.whl (61 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 61.5/61.5 KB 11.7 MB/s eta 0:00:00
Collecting certifi>=2017.4.17
  Downloading certifi-2022.12.7-py3-none-any.whl (155 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 155.3/155.3 KB 12.0 MB/s eta 0:00:00
Collecting ply
  Downloading ply-3.11-py2.py3-none-any.whl (49 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 49.6/49.6 KB 22.7 MB/s eta 0:00:00
Collecting wcwidth>=0.1.7
  Downloading wcwidth-0.2.6-py2.py3-none-any.whl (29 kB)
Collecting pyperclip>=1.6
  Downloading pyperclip-1.8.2.tar.gz (20 kB)
  Preparing metadata (setup.py): started
  Preparing metadata (setup.py): finished with status 'done'
Collecting cffi>=1.12
  Downloading cffi-1.15.1-cp310-cp310-manylinux_2_17_x86_64.manylinux2014_x86_64.whl (441 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 441.8/441.8 KB 11.6 MB/s eta 0:00:00
Collecting jsonpointer>=1.9
  Downloading jsonpointer-2.3-py2.py3-none-any.whl (7.8 kB)
Collecting fasteners>=0.7.0
  Downloading fasteners-0.18-py3-none-any.whl (18 kB)
Collecting rfc3986>=1.2.0
  Downloading rfc3986-2.0.0-py2.py3-none-any.whl (31 kB)
Collecting pycparser
  Downloading pycparser-2.21-py2.py3-none-any.whl (118 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 118.7/118.7 KB 12.5 MB/s eta 0:00:00
Building wheels for collected packages: murano-pkg-check, PrettyTable, netifaces, pyinotify, pyperclip
  Building wheel for murano-pkg-check (setup.py): started
  Building wheel for murano-pkg-check (setup.py): finished with status 'done'
  Created wheel for murano-pkg-check: filename=murano_pkg_check-0.3.0-py3-none-any.whl size=51982 sha256=faddd0e2c0bba54d8a22379e8b54619846c0ae1557f03f240085cb8d5234560f
  Stored in directory: /root/.cache/pip/wheels/57/81/0a/73dd84423cba563ba7903e3bf498ede5aef2998bc1453e4e73
  Building wheel for PrettyTable (setup.py): started
  Building wheel for PrettyTable (setup.py): finished with status 'done'
  Created wheel for PrettyTable: filename=prettytable-0.7.2-py3-none-any.whl size=13714 sha256=1564bc58db4c18a1573f90265d7948830166bc591f6a49d1842b34fe2e6e5179
  Stored in directory: /root/.cache/pip/wheels/25/4b/07/18c5d92824315576e478206ea69df34a9e31958f6143eb0e31
  Building wheel for netifaces (setup.py): started
  Building wheel for netifaces (setup.py): finished with status 'done'
  Created wheel for netifaces: filename=netifaces-0.11.0-cp310-cp310-linux_x86_64.whl size=35029 sha256=4725285ca0d51a33b80dc9784cb45c5b4d9f0b70bdb3a001492654e9664ac8cf
  Stored in directory: /root/.cache/pip/wheels/48/65/b3/4c4cc6038b81ff21cc9df69f2b6774f5f52e23d3c275ed15aa
  Building wheel for pyinotify (setup.py): started
  Building wheel for pyinotify (setup.py): finished with status 'done'
  Created wheel for pyinotify: filename=pyinotify-0.9.6-py3-none-any.whl size=25352 sha256=673937584e784eaa549c2ada495ecd012314932e164be1e23ce46d31d8238bb9
  Stored in directory: /root/.cache/pip/wheels/c8/e6/94/d10115432935b5daf653cc1341686f2b6b033d9341d4d32911
  Building wheel for pyperclip (setup.py): started
  Building wheel for pyperclip (setup.py): finished with status 'done'
  Created wheel for pyperclip: filename=pyperclip-1.8.2-py3-none-any.whl size=11137 sha256=a3e7e121ee0dd4f8e871e93625f996c95e33ecc41216ac3d58ffe8c0f5cae0f9
  Stored in directory: /root/.cache/pip/wheels/04/24/fe/140a94a7f1036003ede94579e6b4227fe96c840c6f4dcbe307
Successfully built murano-pkg-check PrettyTable netifaces pyinotify pyperclip
Installing collected packages: wcwidth, requestsexceptions, pytz, pyperclip, pyinotify, PrettyTable, ply, netifaces, netaddr, msgpack, appdirs, wrapt, websocket-client, WebOb, urllib3, ujson, simplejson, semantic-version, rfc3986, PyYAML, python-dateutil, pyrsistent, pydot, pycparser, pbr, packaging, networkx, jsonpointer, jmespath, iso8601, idna, futurist, fasteners, decorator, charset-normalizer, certifi, Babel, autopage, attrs, yaql, stevedore, requests, oslo.i18n, os-service-types, jsonschema, jsonpatch, debtcollector, cmd2, cffi, warlock, python-swiftclient, oslo.utils, oslo.context, oslo.config, murano-pkg-check, keystoneauth1, dogpile.cache, docker, cryptography, cliff, python-ironic-inspector-client, python-cinderclient, pyOpenSSL, oslo.serialization, oslo.concurrency, openstacksdk, gnocchiclient, python-novaclient, python-keystoneclient, python-glanceclient, python-barbicanclient, osprofiler, oslo.log, osc-lib, os-client-config, python-zaqarclient, python-watcherclient, python-vitrageclient, python-openstackclient, python-neutronclient, python-muranoclient, python-mistralclient, python-ironicclient, python-heatclient, python-designateclient, python-congressclient, aodhclient, python-zunclient, python-troveclient, python-senlinclient, python-searchlightclient, python-saharaclient, python-octaviaclient, openstackclient
  Attempting uninstall: cryptography
    Found existing installation: cryptography 3.4.8
    Not uninstalling cryptography at /usr/lib/python3/dist-packages, outside environment /usr
    Can't uninstall 'cryptography'. No files were found to uninstall.
Successfully installed Babel-2.12.1 PrettyTable-0.7.2 PyYAML-6.0 WebOb-1.8.7 aodhclient-3.3.0 appdirs-1.4.4 attrs-23.1.0 autopage-0.5.1 certifi-2022.12.7 cffi-1.15.1 charset-normalizer-3.1.0 cliff-4.2.0 cmd2-2.4.3 cryptography-40.0.2 debtcollector-2.5.0 decorator-5.1.1 docker-6.0.1 dogpile.cache-1.1.8 fasteners-0.18 futurist-2.4.1 gnocchiclient-7.0.8 idna-3.4 iso8601-1.1.0 jmespath-1.0.1 jsonpatch-1.32 jsonpointer-2.3 jsonschema-4.17.3 keystoneauth1-5.1.2 msgpack-1.0.5 murano-pkg-check-0.3.0 netaddr-0.8.0 netifaces-0.11.0 networkx-3.1 openstackclient-4.0.0 openstacksdk-1.0.1 os-client-config-2.1.0 os-service-types-1.7.0 osc-lib-2.7.0 oslo.concurrency-5.1.1 oslo.config-9.1.1 oslo.context-5.1.1 oslo.i18n-6.0.0 oslo.log-5.2.0 oslo.serialization-5.1.1 oslo.utils-6.1.0 osprofiler-3.4.3 packaging-23.1 pbr-5.11.1 ply-3.11 pyOpenSSL-23.1.1 pycparser-2.21 pydot-1.4.2 pyinotify-0.9.6 pyperclip-1.8.2 pyrsistent-0.19.3 python-barbicanclient-5.5.0 python-cinderclient-9.3.0 python-congressclient-2.0.1 python-dateutil-2.8.2 python-designateclient-5.2.0 python-glanceclient-4.3.0 python-heatclient-3.2.0 python-ironic-inspector-client-4.9.0 python-ironicclient-5.1.0 python-keystoneclient-5.1.0 python-mistralclient-5.0.0 python-muranoclient-2.6.0 python-neutronclient-9.0.0 python-novaclient-18.3.0 python-octaviaclient-3.4.0 python-openstackclient-6.2.0 python-saharaclient-4.1.0 python-searchlightclient-2.1.1 python-senlinclient-3.0.0 python-swiftclient-4.3.0 python-troveclient-8.1.0 python-vitrageclient-4.7.0 python-watcherclient-4.1.0 python-zaqarclient-2.5.1 python-zunclient-4.6.0 pytz-2023.3 requests-2.28.2 requestsexceptions-1.4.0 rfc3986-2.0.0 semantic-version-2.10.0 simplejson-3.19.1 stevedore-5.0.0 ujson-5.7.0 urllib3-1.26.15 warlock-2.0.1 wcwidth-0.2.6 websocket-client-1.5.1 wrapt-1.15.0 yaql-2.0.0
WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv
--> 559be355d41
STEP 5/9: WORKDIR /tmp
--> 6614c4e5a63
STEP 6/9: RUN git clone https://gitlab.ics.muni.cz/cloud/g2/openstack-infrastructure-as-code-automation.git
Cloning into 'openstack-infrastructure-as-code-automation'...
--> 6925860644f
STEP 7/9: RUN mkdir /tmp/ac
--> dc93e7ec5ac
STEP 8/9: ADD prod-metaseminar-hands-on-2023-04-28-openrc.sh.inc /tmp/ac/
--> 3f1ae8778ee
STEP 9/9: RUN openstack --version &&     terraform --version &&     git --version &&     pwd &&     ls -la
openstack 6.2.0
Terraform v1.4.5
on linux_amd64
git version 2.34.1
/tmp
total 24
drwxrwxrwt. 1 root root 4096 Apr 26 12:47 .
dr-xr-xr-x. 1 root root 4096 Apr 26 12:47 ..
drwxr-xr-x. 1 root root 4096 Apr 26 12:47 ac
drwxr-xr-x. 4 root root 4096 Apr 26 12:47 openstack-infrastructure-as-code-automation
COMMIT registry.gitlab.ics.muni.cz:443/246254/metaseminar-hands-on-2023-04-28/hands-on-tools:latest
--> 19d40fec143
Successfully tagged registry.gitlab.ics.muni.cz:443/246254/metaseminar-hands-on-2023-04-28/hands-on-tools:latest
19d40fec143fed47ae203fa19c7f22ed5520beef2dd5acd66a1028c197f79b7c
[freznicek@lenovo-t14 containerimage 0]$ podman push registry.gitlab.ics.muni.cz:443/246254/metaseminar-hands-on-2023-04-28/hands-on-tools:latest
Getting image source signatures
Copying blob ce1986a8cf5c done
Copying blob 078348c2ef67 done
Copying blob 401327be40de done
Copying blob 4351f235b797 done
Copying blob c435dad7d465 done
Copying blob b84fbdbf6d11 skipped: already exists
Copying blob a86226368b4e done
Copying blob b42ba682a740 done
Copying config 19d40fec14 done
Writing manifest to image destination
Storing signatures
[freznicek@lenovo-t14 containerimage 0]$
```
