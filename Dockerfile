FROM ubuntu:jammy

# standard packages
RUN apt -y update && \
    apt -y install vim git-core util-linux python3-minimal python3-pip sshuttle iptables bash-completion net-tools apt-transport-https software-properties-common gnupg2 curl openssh-client ncat mc

# terraform
RUN curl https://apt.releases.hashicorp.com/gpg | gpg --dearmor > hashicorp.gpg && \
    install -o root -g root -m 644 hashicorp.gpg /etc/apt/trusted.gpg.d/ && \
    apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main" && \
    apt install terraform

# openstack client
RUN python3 -mpip install openstackclient

WORKDIR /tmp

# get terraform recipe
RUN git clone https://gitlab.ics.muni.cz/cloud/g2/openstack-infrastructure-as-code-automation.git

# add app.credentials
RUN mkdir /tmp/ac
ADD prod-metaseminar-hands-on-2023-04-28-openrc.sh.inc /tmp/ac/

# summary
RUN openstack --version && \
    terraform --version && \
    git --version && \
    pwd && \
    ls -la
